# Changelog
All notable changes to this project will be documented in this file. Format inspired by http://keepachangelog.com/ and this example https://github.com/olivierlacan/keep-a-changelog/blob/master/CHANGELOG.md

## [1.0.2] - 2018-02-15

### Removed

- Logging from production

## [1.0] - 2017-08-23

### Added

- First version of Pocket Up
