# Pocket Up

Pocket Up is an Android app that *wakes up* your phone - switches the screen on - when you take it out of your pocket or purse or open the lid if you use one.

Pocket Up is a [WaveUp](https://www.gitlab.com/juanitobananas/waveup/) fork with an even [KISS](https://en.wikipedia.org/wiki/KISS_principle)er attitude. Forked, stripped down to one option, and with a cooler icon. It will never[^1] do anything else.

## Required Android Permissions:

- WAKE_LOCK to turn on the screen.
- RECEIVE_BOOT_COMPLETED to automatically startup on boot if enabled.

## Known issues

### Might not work properly on MIUI-based ROMs

It actually *does* work. MIUI uses a task manager and a battery manager. Just add Pocket Up to the whitelists:

- `Security` → `Permissions` → `Autostart` and select `Pocket Up`
- `Settings` → `Battery & performance` → `Manager apps battery usage` → `Choose apps` → `Pocket Up` and select `No restriction`.

### (Possible) battery drain

Unfortunately, some smartphones let the CPU on while listening to the proximity sensor. This is called a *wake lock* and causes considerable battery drain. This isn't my fault and I cannot do anything to change this. Other phones will 'go to sleep' when the screen is turned off while still listening to the proximity sensor. In this case, the battery drain is negligible.

## Acknowledgments

My special thanks to (these actually apply more to WaveUp, but nonetheless):

- **Tsuyoshi** and **Naofumi** for the Japanese translation.
- **Licaon_Kter** for the Romanian translation.
- **ko_lo**, **askthedust** and **Cedric Octave** for the French translation.
- **gidano** (sympda.blog.hu) for the Hungarian translation.
- **Chorus Pocus** for the Brazilian Portuguese translation.
- **Jeroen** for the Dutch translation.
- **Aitor Beriain** for the Basque translation.
- **Christo Agveriandika** for the Indonesian translation.
- **Ole Carlsen** for the Danish translation.

## Download it

[<img src="f-droid/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/com.jarsilio.android.pocketup)

[<img src="google-play-store/google-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.jarsilio.android.pocketup)

## Legal Attributions

Google Play and the Google Play logo are trademarks of Google Inc.

[^1]: My *nevers* are known to be pretty weak. So who nows? I might as well end up stripping down Pocket Up at some point :)
