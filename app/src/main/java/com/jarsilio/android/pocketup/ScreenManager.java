/*
 * Copyright (c) 2016-2017 Juan García Basilio
 *
 * This file is part of Pocket Up.
 *
 * Pocket Up is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pocket Up is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pocket Up.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.pocketup;

import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

import timber.log.Timber;

public class ScreenManager {
    private static final long TIME_SCREEN_ON = 5000;

    private final PowerManager powerManager;
    private final PowerManager.WakeLock wakeLock;
    private final Context context;

    private static volatile ScreenManager instance;

    public static ScreenManager getInstance(Context context) {
        if (instance == null ) {
            synchronized (ScreenManager.class) {
                if (instance == null) {
                    instance = new ScreenManager(context);
                }
            }
        }

        return instance;
    }

    private ScreenManager(Context context) {
        this.powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        this.wakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "WakeUpWakeLock");
        this.context = context;
    }

    public void turnOnScreen() {
        if (!isScreenOn()) {
            Timber.i("Turning screen on");
            if (wakeLock.isHeld()) {
                wakeLock.release();
            }
            wakeLock.acquire(TIME_SCREEN_ON);
        }
    }

    public boolean isScreenOn() {
        boolean screenOn;
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) { // isScreenOn method is deprecated from API Level 20
            screenOn = powerManager.isInteractive();
        } else {
            screenOn = powerManager.isScreenOn();
        }

        return screenOn;
    }

}
