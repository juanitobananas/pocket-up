package com.jarsilio.android.pocketup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jarsilio.android.privacypolicy.PrivacyPolicyBuilder;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    private Button enableButton;
    private ImageView pocketUpImage;
    private TextView statusText;

    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prepareViewItems();

        settings = Settings.getInstance(getApplicationContext());
        startOrStopServiceAndUpdateUI();
    }

    private void startOrStopServiceAndUpdateUI() {
        App app = (App) getApplication();
        if (settings.isServiceEnabled()) {
            // If for some reason the service is not running it will start when you start Pocket Up
            app.startPocketUp();
        } else {
            app.stopPocketUp();
        }

        updateUI();
    }

    private void prepareViewItems() {
        pocketUpImage = (ImageView) findViewById(R.id.pocket_up_state_image);
        statusText = (TextView) findViewById(R.id.status_text);

        enableButton = (Button) findViewById(R.id.button_enable);
        enableButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                App app = (App) getApplication();
                if (settings.isServiceEnabled()) {
                    // It *was* enabled (the user tapped on 'disable')
                    app.stopPocketUp();
                    settings.setServiceEnabled(false);
                } else {
                    app.startPocketUp();
                    settings.setServiceEnabled(true);
                }

                updateUI();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        startOrStopServiceAndUpdateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void updateUI() {
        if (settings.isServiceEnabled()) {
            enableButton.setText(R.string.disable);
            pocketUpImage.setImageResource(R.drawable.pocket_up_big);
            statusText.setText(R.string.wave_up_service_started);
        } else {
            enableButton.setText(R.string.pref_enable);
            pocketUpImage.setImageResource(R.drawable.pocket_up_big_gray);
            statusText.setText(R.string.wave_up_service_stopped);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.privacy_policy_menu_item:
                showPrivacyPolicyActivity();
                break;
            case R.id.licenses_menu_item:
                showAboutLicensesActivity();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showPrivacyPolicyActivity() {
        new PrivacyPolicyBuilder()
                .withIntro(getString(R.string.app_name), "Juan García Basilio (juanitobananas)")
                .withUrl("https://gitlab.com/juanitobananas/pocket-up/blob/master/PRIVACY.md#pocket-up-privacy-policy")
                .withMeSection()
                .withAutoGoogleOrFDroidSection()
                .withEmailSection("juam+pocketup@posteo.net")
                .start(getApplicationContext());
    }

    private void showAboutLicensesActivity() {
        new LibsBuilder()
                //provide a style (optional) (LIGHT, DARK, LIGHT_DARK_TOOLBAR)
                .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
                .withAboutIconShown(true)
                .withAboutVersionShown(true)
                .withActivityTitle(getString(R.string.licenses_title))
                .withAboutDescription(getString(R.string.licenses_about_libraries_text))
                //.withAboutDescription("This is a small sample which can be set in the about my app description file.<br /><b>You can style this with html markup :D</b>")
                //start the activity
                .start(getApplicationContext());
    }
}
