/*
 * Copyright (c) 2016 Juan García Basilio
 *
 * This file is part of Pocket Up.
 *
 * Pocket Up is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pocket Up is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pocket Up.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.pocketup;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class Settings {
    public static final String ENABLED = "pref_enable";

    private static volatile Settings instance;
    private final Context context;

    public static Settings getInstance(Context context) {
        if (instance == null ) {
            synchronized (Settings.class) {
                if (instance == null) {
                    instance = new Settings(context);
                }
            }
        }

        return instance;
    }

    private Settings(Context context) {
        this.context = context;
    }
    
    public SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isServiceEnabled() {
        return getPreferences().getBoolean(ENABLED, false);
    }

    private void setPreference(String key, boolean value) {
        getPreferences().edit().putBoolean(key, value).commit();
    }

    public void setServiceEnabled(boolean enabled) {
        setPreference(ENABLED, enabled);
    }
}
