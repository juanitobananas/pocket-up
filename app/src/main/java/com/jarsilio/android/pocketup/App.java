package com.jarsilio.android.pocketup;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import com.jarsilio.android.pocketup.receivers.ScreenReceiver;

import timber.log.Timber;

public class App extends Application {
    private ProximitySensorManager proximitySensorManager;
    private ScreenReceiver screenReceiver;

    private boolean started = false;

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.v("Pocket Up App 'created' by system");

        proximitySensorManager = ProximitySensorManager.getInstance(getApplicationContext());
        screenReceiver = new ScreenReceiver();

        Timber.plant(new LongTagTree(getPackageName()));
    }

    public void startPocketUp() {
        if (!started) {
            Timber.i("Starting Pocket Up Service");
            proximitySensorManager.startOrStopListeningDependingOnConditions();
            registerScreenReceiver();
            started = true;
        }
    }

    public void stopPocketUp() {
        if (started) {
            Timber.i("Stopping Pocket Up Service");
            unregisterReceiver(screenReceiver);
            proximitySensorManager.stop();
            started = false;
        }
    }

    private void registerScreenReceiver() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(screenReceiver, filter);
    }

    public class LongTagTree extends Timber.DebugTree {
        private static final int MAX_TAG_LENGTH = 23;
        private final String packageName;

        public LongTagTree(String packageName) {
            this.packageName = packageName;
        }

        protected String getMessage(String tag, String message) {
            String newMessage;
            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                // Tag length limitation (<23): Use truncated package name and add class name to message
                newMessage = String.format("%s: %s", tag, message);
            } else {
                // No tag length limit limitation: Use package name *and* class name
                newMessage = message;
            }
            return newMessage;
        }

        protected String getTag(String tag) {
            String newTag;
            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                // Tag length limitation (<23): Use truncated package name and add class name to message
                newTag = packageName;
                if (newTag.length() > MAX_TAG_LENGTH) {
                    newTag = "..." + packageName.substring(packageName.length() - MAX_TAG_LENGTH + 3, packageName.length());
                }
            } else {
                // No tag length limit limitation: Use package name *and* class name
                newTag = String.format("%s (%s)", packageName, tag);
            }

            return newTag;
        }

        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            String newMessage = getMessage(tag, message);
            String newTag = getTag(tag);

            super.log(priority, newTag, newMessage, t);
        }
    }
}
