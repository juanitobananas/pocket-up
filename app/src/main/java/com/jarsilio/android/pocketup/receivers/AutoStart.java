/*
 * Copyright (c) 2016-2018 Juan García Basilio
 *
 * This file is part of Pocket Up.
 *
 * Pocket Up is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pocket Up is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pocket Up.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.pocketup.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jarsilio.android.pocketup.App;
import com.jarsilio.android.pocketup.Settings;

import timber.log.Timber;

public class AutoStart extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Settings.getInstance(context).isServiceEnabled()) {
            if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                Timber.d("Received ACTION_BOOT_COMPLETED.");
                startPocketUpService(context);
            } else if (intent.getAction().equals(Intent.ACTION_MY_PACKAGE_REPLACED)) {
                Timber.d("Received ACTION_MY_PACKAGE_REPLACED.");
                startPocketUpService(context);
            }
        }
    }

    private void startPocketUpService(Context context) {
        App app = (App) context.getApplicationContext();
        app.startPocketUp();
    }
}
