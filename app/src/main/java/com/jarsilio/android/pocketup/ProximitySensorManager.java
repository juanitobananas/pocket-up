/*
 * Copyright (c) 2016-2017 Juan García Basilio
 *
 * This file is part of Pocket Up.
 *
 * Pocket Up is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pocket Up is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pocket Up.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.pocketup;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.Locale;

import timber.log.Timber;

public class ProximitySensorManager implements SensorEventListener {
    private final SensorManager sensorManager;
    private final Sensor proximitySensor;

    private enum Distance { NEAR, FAR }
    private Distance lastDistance = Distance.FAR;
    private long lastTime = 0;
    private static final long POCKET_THRESHOLD = 5000;

    private static volatile ProximitySensorManager instance;
    private final ScreenManager screenManager;

    private boolean listening = false;

    public static ProximitySensorManager getInstance(Context context) {
        if (instance == null ) {
            synchronized (ProximitySensorManager.class) {
                if (instance == null) {
                    instance = new ProximitySensorManager(context);
                }
            }
        }

        return instance;
    }

    private ProximitySensorManager(Context context) {
        this.screenManager = ScreenManager.getInstance(context);
        this.sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        this.proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    private void start() {
        if (!listening) {
            Timber.d("Registering proximity sensor listener.");
            sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
            listening = true;
        } else {
            Timber.d("Proximity sensor listener is already registered. There is no need to register it again.");
        }
    }

    public final void startOrStopListeningDependingOnConditions() {
        if (screenManager.isScreenOn()) {
            Timber.d("Stopping proximity sensor because the screen is on.");
            stop();
        } else {
            Timber.d("Starting proximity sensor because the screen is off.");
            start();
        }
    }

    public final void stop() {
        if (listening) {
            Timber.d("Unregistering proximity sensor listener");
            sensorManager.unregisterListener(this);
            listening = false;
        } else {
            Timber.d("Proximity sensor listener is already unregistered. There is no need to unregister it again.");
        }
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        long currentTime = System.currentTimeMillis();
        Distance currentDistance = event.values[0] >= event.sensor.getMaximumRange() ? Distance.FAR : Distance.NEAR;
        Timber.v(String.format(Locale.ENGLISH, "Proximity sensor changed: %s (current sensor value: %f - max. sensor value: %f)", currentDistance, event.values[0], event.sensor.getMaximumRange()));

        long timeBetweenFarAndNear = currentTime - lastTime;
        boolean tookOutOfPocket = timeBetweenFarAndNear > POCKET_THRESHOLD;
        boolean uncovered = lastDistance == Distance.NEAR && currentDistance == Distance.FAR;

        if (uncovered) {
            Timber.d("Uncovered proximity sensor. Time it was covered: " + timeBetweenFarAndNear + " ms. Current POCKET_THRESHOLD: " + POCKET_THRESHOLD);
            if (tookOutOfPocket) {
                Timber.d("Covered more than POCKET_THRESHOLD. Turning screen on (if it's not already on)");
                screenManager.turnOnScreen();
            }
        }

        lastDistance = currentDistance;
        lastTime = currentTime;
    }


    @Override
    public final void onAccuracyChanged(Sensor sensor, int i) {
    }
}