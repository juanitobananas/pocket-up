/*
 * Copyright (c) 2016-2018 Juan García Basilio
 *
 * This file is part of Pocket Up.
 *
 * Pocket Up is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pocket Up is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pocket Up.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.pocketup.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jarsilio.android.pocketup.ProximitySensorManager;

import timber.log.Timber;

public class ScreenReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) { // I am leaving this if just for the logging
            Timber.d("Screen off");
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Timber.d("Screen on");
        }
        ProximitySensorManager.getInstance(context).startOrStopListeningDependingOnConditions();
    }
}